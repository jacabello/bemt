function varargout = Interface(varargin)
% INTERFACE MATLAB code for Interface.fig
%      INTERFACE, by itself, creates a new INTERFACE or raises the existing
%      singleton*.
%
%      H = INTERFACE returns the handle to a new INTERFACE or the handle to
%      the existing singleton*.
%
%      INTERFACE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in INTERFACE.M with the given input arguments.
%
%      INTERFACE('Property','Value',...) creates a new INTERFACE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Interface_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Interface_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Interface

% Last Modified by GUIDE v2.5 10-Jun-2022 09:30:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Interface_OpeningFcn, ...
                   'gui_OutputFcn',  @Interface_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Interface is made visible.
function Interface_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Interface (see VARARGIN)

% Choose default command line output for Interface
handles.output = hObject;

% set(handles.uipanel_rotor_calc,'visible','off');
set(handles.uipanel_xfoil,'visible','off')
set(handles.uipanel_3D,'visible','off')
set(handles.popupmenu_3Dplot,'visible','off');
set(handles.popupmenu_plotlist,'visible','on');

handles.dirMain = pwd;
handles.dirXfoil = [handles.dirMain,'/XFoil'];

% Update handles structure
handles.rotor.R = 100;
handles.rotor.Nb = 1;
handles.rotor.hub = 10;
handles.rotor.c_fix = 30;
handles.rotor.input_angle = 1;
handles.rotor.r = 0;
handles.rotor.hole = 4;
handles.rotor.hubFit = 0;
handles.rotor.hubHeight = 20;

handles.flow.heli = 1;
handles.flow.Re = 8e4;
handles.flow.Vo = 0;
handles.flow.nu = 1;
handles.flow.f = 6;
handles.flow.omega = handles.flow.f*2*pi;
handles.flow.Re = handles.flow.omega * handles.rotor.c_fix * ...
  handles.rotor.R / handles.flow.nu;

%Default value for lift. Lienar theory
alpha = -20:10;
cL = 0.5+alpha*0.12;  cD = zeros(length(alpha),1);
handles.AirfoilData = [alpha',cL',cD];


handles.param.tol = 1e-16;      %Tolerance for the loop
handles.param.TipC = 0;         %Tip Correction 1 true
handles.param.Corr_3D = 0;      %3D correction
handles.param.relax = 0.35;     %Relaxation factor
handles.param.Limit = 1000;  
handles.param.Mode = 'Design';   
handles.param.Lambda_i_guess=0.1;
handles.param.ap_guess=0;


% default xfoil parameters
handles.XF.Re_min = 2e4;
handles.XF.dRe = 1e4;
handles.XF.Re_max = 8e4;
handles.XF.alpha_min = -11;
handles.XF.dalpha = 0.5;
handles.XF.alpha_max = 11;

handles.XF.Airfoil = 'NACA0012';
handles.XF.Nc = 5;
handles.XF.Mach = 0.2;



guidata(hObject, handles);

% UIWAIT makes Interface wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Interface_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%==========================================================================
%% ROTOR PARAMETERS MENU
%==========================================================================

%Rotor Menu
function edit_R_Callback(hObject, eventdata, handles)
% hObject    handle to edit_R (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_R as text
%        str2double(get(hObject,'String')) returns contents of edit_R as a double
handles.rotor.R  = str2num(get(hObject,'String'));

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_R_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_R (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_Nb_Callback(hObject, eventdata, handles)
% hObject    handle to edit_Nb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_Nb as text
%        str2double(get(hObject,'String')) returns contents of edit_Nb as a double
handles.rotor.Nb = str2num(get(hObject,'String'));
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit_Nb_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_Nb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_hub_Callback(hObject, eventdata, handles)
% hObject    handle to edit_hub (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_hub as text
%        str2double(get(hObject,'String')) returns contents of edit_hub as a double
handles.rotor.hub = str2num(get(hObject,'String'));
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit_hub_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_hub (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_chord_Callback(hObject, eventdata, handles)
% hObject    handle to edit_chord (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_chord as text
%        str2double(get(hObject,'String')) returns contents of edit_chord as a double
handles.rotor.c_fix = str2num(get(hObject,'String'))
handles.rotor.r = 0;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit_chord_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_chord (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_AoA_Callback(hObject, eventdata, handles)
% hObject    handle to edit_AoA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_AoA as text
%        str2double(get(hObject,'String')) returns contents of edit_AoA as a double
handles.rotor.input_angle = str2num(get(hObject,'String'));
handles.rotor.r = 0;
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_AoA_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_AoA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton_loadProfile.
function pushbutton_loadProfile_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_loadProfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[Fieldfilename,Fieldpathname]=uigetfile('*.mat','Load a file :');
fullFileName = fullfile(Fieldpathname, Fieldfilename);
temp = load(fullFileName);

handles.rotor = temp.rotor;
handles.rotor.input_angle = temp.rotor.alpha;

set(handles.edit_R, 'String', handles.rotor.R);
set(handles.edit_Nb, 'String', handles.rotor.Nb);
set(handles.edit_hub, 'String', handles.rotor.hub);
set(handles.edit_chord, 'String', '-');
set(handles.edit_AoA, 'String', '-');

guidata(hObject, handles);



% --- Executes on button press in pushbutton_loadBlade.
function pushbutton_loadBlade_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_loadBlade (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[Fieldfilename,Fieldpathname]=uigetfile('*.mat','Load a file :');
fullFileName = fullfile(Fieldpathname, Fieldfilename);
temp = load(fullFileName);

handles.rotor = temp.rotor;


%========================================================================
%% FLOW PARAMETERS
%========================================================================

% --- Executes on selection change in popupmenu2.
function popupmenu_heli_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
rotor_type = get(hObject,'Value');
if (rotor_type==1)
    handles.flow.helicopter = 1;
else
    handles.flow.helicopter = 0;
    flow.Vo = -abs(flow.Vo);
end
guidata(hObject, handles);
    
% --- Executes during object creation, after setting all properties.
function popupmenu_heli_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_freqz_Callback(hObject, eventdata, handles)
% hObject    handle to edit_freqz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_freqz as text
%        str2double(get(hObject,'String')) returns contents of edit_freqz as a double
handles.flow.f = str2num(get(hObject,'String'));
handles.flow.omega = handles.flow.f*2*pi;

temp_struct = handles.rotor;
if (isfield(temp_struct, 'c'))
    handles.flow.Re = handles.flow.omega * handles.rotor.c(end) * ...
     handles.rotor.R / handles.flow.nu;
else
    handles.flow.Re = handles.flow.omega * handles.rotor.c_fix * ...
     handles.rotor.R / handles.flow.nu;
end
set(handles.edit_Re, 'String', handles.flow.Re);

guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit_freqz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_freqz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_Re_Callback(hObject, eventdata, handles)
% hObject    handle to edit_Re (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_Re as text
%        str2double(get(hObject,'String')) returns contents of edit_Re as a double
handles.flow.Re = str2num(get(hObject,'String'));

temp_struct = handles.rotor;
if (isfield(temp_struct, 'c'))
    handles.flow.omega = handles.flow.Re*handles.flow.nu/(handles.rotor.c(end)*handles.rotor.R);     %Omega to get the aproximate reynodls number
else
    handles.flow.omega = handles.flow.Re*handles.flow.nu/(handles.rotor.c_fix*handles.rotor.R);     %Omega to get the aproximate reynodls number
end
handles.flow.f = handles.flow.omega/2/pi;

set(handles.edit_freqz, 'String', handles.flow.f);

guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit_Re_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_Re (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_vel_Callback(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit11 as text
%        str2double(get(hObject,'String')) returns contents of edit11 as a double
handles.flow.Vo = str2double(get(hObject,'String'));
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit_vel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_viscosity_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double
handles.flow.nu = str2double(get(hObject,'String'));


temp_struct = handles.rotor;
if (isfield(temp_struct, 'c'))
    handles.flow.Re = handles.flow.omega * handles.rotor.c(end) * ...
     handles.rotor.R / handles.flow.nu;
else
    handles.flow.Re = handles.flow.omega * handles.rotor.c_fix * ...
     handles.rotor.R / handles.flow.nu;
end
set(handles.edit_Re, 'String', handles.flow.Re);
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit_viscosity_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton10.
function pushbutton_loadXfoil_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[Fieldfilename,Fieldpathname]=uigetfile('*.mat','Load a file :');
fullFileName = fullfile(Fieldpathname, Fieldfilename);
tempAeroData = load(fullFileName);
handles.AirfoilData = tempAeroData.AirfoilData;
guidata(hObject,handles);
%==========================================================================
%% SIMULATION PARAMETERS
%==========================================================================


function edit_tol_Callback(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit17 as text
%        str2double(get(hObject,'String')) returns contents of edit17 as a double
handles.param.tolerance = str2double(get(hObject,'String'));
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit_tol_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_maxIter_Callback(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit16 as text
%        str2double(get(hObject,'String')) returns contents of edit16 as a double
handles.param.Limit = str2double(get(hObject,'String'));
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit_maxIter_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu4.
function popupmenu_tip_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu4
handles.param.TipC = get(hObject,'Value')-1;
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function popupmenu_tip_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu5.
function popupmenu_corr3D_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu5 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu5
handles.param.Corr_3D = get(hObject,'Value')-1;
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_corr3D_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_relax_Callback(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit18 as text
%        str2double(get(hObject,'String')) returns contents of edit18 as a double
handles.param.relax = str2double(get(hObject,'String'));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit_relax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_lambda0_Callback(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit23 as text
%        str2double(get(hObject,'String')) returns contents of edit23 as a double
handles.param.Lambda_i_guess = str2double(get(hObject,'String'));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit_lambda0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_ap0_Callback(hObject, eventdata, handles)
% hObject    handle to edit22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit22 as text
%        str2double(get(hObject,'String')) returns contents of edit22 as a double
handles.param.ap_guess = str2double(get(hObject,'String'));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit_ap0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
%==========================================================================
%% EXECUTION BUTTONS
%==========================================================================

% --- Executes on button press in pushbutton_Run.
function pushbutton_Run_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.rotor.r == 0
    %Radius distribution
    handles.rotor.r = linspace(0.2*handles.rotor.R+handles.rotor.R/100, ...
        handles.rotor.R-handles.rotor.R/100,100);   
    %Chord distribution
    handles.rotor.c = handles.rotor.c_fix*ones(1,length(handles.rotor.r)); 
    %input angle
    handles.rotor.input_angle = ones(1,length(handles.rotor.r))*handles.rotor.input_angle;
end


% tip solidity
handles.flow.sigma = handles.rotor.Nb*handles.rotor.c/pi/handles.rotor.R;           
handles.flow.Lambda_c = handles.flow.Vo/handles.flow.omega/handles.rotor.R;     

% run simulation
[handles.results] = BEMT(handles.rotor, handles.flow,...
    handles.AirfoilData, handles.param);

handles.rotor.theta = handles.results.theta;

guidata(hObject,handles);

% --- Executes on button press in pushbutton_saveResults.
function pushbutton_saveResults_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_saveResults (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
temp_struct = handles;
if (not(isfield(handles,'results')))
    f = warndlg('No results to save','Warning');
else
    rotor = handles.rotor;
    flow = handles.flow;
    results = handles.results;
    [file,path,indx] = uiputfile('Blade_Results.mat');
    fullFile = fullfile(path,file);
    save(fullFile,'rotor','flow','results')
end



% --- Executes on button press in pushbutton_loadResults.
function pushbutton_loadResults_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_loadResults (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[Fieldfilename,Fieldpathname]=uigetfile('*.mat','Load a file :');
fullFileName = fullfile(Fieldpathname, Fieldfilename);
temp = load(fullFileName);

handles.rotor = temp.rotor;
handles.flow = temp.flow;
handles.results = temp.results;
guidata(hObject,handles);

%==========================================================================
%% TOP MENU
%==========================================================================

% --- Executes on button press in pushbutton1.
function pushbutton_design_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.param.Mode = 'Design'; 
uistack(handles.uipanel_rotor,'top');
set(handles.uipanel_3D,'visible','off');
uistack(handles.uipanel_3D,'bottom');
set(handles.uipanel_rotor,'visible','on');
set(handles.uipanel_fluid_param,'visible','on');
set(handles.uipanel_sim_param,'visible','on');
set(handles.pushbutton_Run,'visible','on');
set(handles.pushbutton_saveResults,'visible','on');
set(handles.pushbutton_loadResults,'visible','on');
set(handles.uipanel_rotor_calc,'visible','off');

set(handles.popupmenu_3Dplot,'visible','off');
set(handles.popupmenu_plotlist,'visible','on');

guidata(hObject,handles);

% --- Executes on button press in pushbutton3.
function pushbutton_calculation_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.param.Mode = 'Calculation'; 
% set(handles.uipanel_rotor,'visible','off');
set(handles.uipanel_3D,'visible','off');
uistack(handles.uipanel_3D,'bottom');
set(handles.uipanel_rotor_calc,'visible','on')
set(handles.uipanel_fluid_param,'visible','on');
set(handles.uipanel_sim_param,'visible','on');
set(handles.pushbutton_Run,'visible','on');
set(handles.pushbutton_saveResults,'visible','on');
set(handles.pushbutton_loadResults,'visible','on');

uistack(handles.uipanel_xfoil,'bottom');
set(handles.uipanel_xfoil,'visible','off');

uistack(handles.uipanel_rotor,'bottom');
uistack(handles.uipanel_rotor_calc,'top');

set(handles.popupmenu_3Dplot,'visible','off');
set(handles.popupmenu_plotlist,'visible','on');
guidata(hObject,handles);

% --- Executes on button press in pushbutton4.
function pushbutton_xfoil_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% set(handles.uipanel_rotor,'visible','off');
set(handles.uipanel_3D,'visible','off');
uistack(handles.uipanel_3D,'bottom');
set(handles.uipanel_xfoil,'visible','on')
uistack(handles.uipanel_rotor_calc,'bottom');
set(handles.uipanel_fluid_param,'visible','off');
set(handles.uipanel_sim_param,'visible','off');
set(handles.pushbutton_Run,'visible','off');
set(handles.pushbutton_saveResults,'visible','off');
set(handles.pushbutton_loadResults,'visible','off');
uistack(handles.uipanel_xfoil,'top');
set(handles.popupmenu_3Dplot,'visible','off');
set(handles.popupmenu_plotlist,'visible','on');
guidata(hObject,handles);



% --- Executes on button press in pushbutton_3D.
function pushbutton_3D_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_3D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.uipanel_3D,'visible','on')
uistack(handles.uipanel_3D,'top');
set(handles.uipanel_xfoil,'visible','off')
uistack(handles.uipanel_rotor_calc,'bottom');
set(handles.uipanel_fluid_param,'visible','off');
set(handles.uipanel_sim_param,'visible','off');
set(handles.pushbutton_Run,'visible','off');
set(handles.pushbutton_saveResults,'visible','off');
set(handles.pushbutton_loadResults,'visible','off');
uistack(handles.uipanel_xfoil,'bottom');
set(handles.popupmenu_3Dplot,'visible','on');
set(handles.popupmenu_plotlist,'visible','off');


guidata(hObject,handles);





% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1



% --- Executes on selection change in popupmenu_plotlist.
function popupmenu_plotlist_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_plotlist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_plotlist contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_plotlist
plotSelection = get(hObject,'Value');

temp_struct = handles;
if (not(isfield(handles,'results')))
    f = warndlg('Run a simulation first','Warning');
else
    if(plotSelection==1)
        axes(handles.axes1)
        plot(handles.rotor.r/handles.rotor.R,handles.results.theta,...
            handles.rotor.r(handles.results.errors)/handles.rotor.R,...
            (1-isnan(handles.results.theta(handles.results.errors))).*...
            handles.results.theta(handles.results.errors),'s',...
            'linewidth',2)
        xlabel('$r/R$','interpreter','Latex','FontSize',18)
        ylabel('$\theta$','interpreter','Latex','FontSize',18)
    elseif(plotSelection==2)
        axes(handles.axes1)
        plot(handles.rotor.r/handles.rotor.R,handles.results.gamma/100,...
            'linewidth',2)
        xlabel('$r/R$','interpreter','Latex','FontSize',18)
        ylabel('$\Gamma (cm^2/s)$','interpreter','Latex','FontSize',18)
    elseif(plotSelection==3)
        plot(handles.rotor.r/handles.rotor.R,handles.results.Lambda_i,...
            handles.rotor.r/handles.rotor.R,handles.results.ap,'linewidth',2)
        xlabel('r/R','interpreter','Latex','Fontsize',14)
        ylabel('Induced Velocity factors','interpreter','LaTex','Fontsize',14)
        legend({'$\lambda_i$','$a_p$'},'location','best',...
            'Interpreter','Latex','Fontsize',14)
    elseif(plotSelection==4)
        plot(handles.rotor.r/handles.rotor.R,handles.results.cL,...
            'linewidth',2)
        xlabel('r/R','interpreter','Latex','Fontsize',14)
        ylabel('Lift Coefficient [CL]','interpreter','LaTex','Fontsize',14)
    elseif(plotSelection==5)  
        plot(handles.rotor.r/handles.rotor.R,handles.results.alpha,...
            'linewidth',2)
        xlabel('r/R','interpreter','Latex','Fontsize',14)
        ylabel('Angle of Attack [deg]','interpreter','LaTex','Fontsize',14)
    elseif(plotSelection==6)  
    end
end


% --- Executes during object creation, after setting all properties.
function popupmenu_plotlist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_plotlist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%==========================================================================
%% 
%==========================================================================


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit21_Callback(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit21 as text
%        str2double(get(hObject,'String')) returns contents of edit21 as a double


% --- Executes during object creation, after setting all properties.
function edit21_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on selection change in popupmenu7.
function popupmenu7_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu7 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu7


% --- Executes during object creation, after setting all properties.
function popupmenu7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function uipanel_fluid_param_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uipanel_fluid_param (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function uipanel_rotor_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uipanel_rotor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function uipanel_rotor_calc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uipanel_rotor_calc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called




function edit_c_Callback(hObject, eventdata, handles)
% hObject    handle to edit_chord (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_chord as text
%        str2double(get(hObject,'String')) returns contents of edit_chord as a double


% --- Executes during object creation, after setting all properties.
function edit_c_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_chord (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_airfoil_Callback(hObject, eventdata, handles)
% hObject    handle to edit_3dairfoil (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_3dairfoil as text
%        str2double(get(hObject,'String')) returns contents of edit_3dairfoil as a double
handles.XF.Airfoil = get(hObject,'String');
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit_airfoil_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_3dairfoil (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_runXfoil.
function pushbutton_runXfoil_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_runXfoil (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%Xfoil Data Extraction 
options = fitoptions('Method','SmoothingSpline',...
                     'SmoothingParam',0.8);    
M1=  waitbar(0,'Please Wait...','Name','Data Extraction from XFOIL');

Re = handles.XF.Re_min:handles.XF.dRe:handles.XF.Re_max;
Alpha = handles.XF.alpha_min:handles.XF.dalpha:handles.XF.alpha_max;

Airfoiltype = handles.XF.Airfoil;
Nc = handles.XF.Nc;

cd(handles.dirXfoil)
for iR=1:length(Re)
    foil = xfoil(Airfoiltype,Alpha,Re(iR),handles.XF.Mach,['oper/vpar n ',num2str(Nc)]);

    Clfit=fit(foil.alpha,foil.CL,'SmoothingSpline',options);
    Cdfit=fit(foil.alpha,foil.CD,'SmoothingSpline',options);
    Cmfit=fit(foil.alpha,foil.Cm,'SmoothingSpline',options);

    AirfoilData(:,1,iR)=Alpha';
    AirfoilData(:,2,iR)=feval(Clfit,Alpha);
    AirfoilData(:,3,iR)=feval(Cdfit,Alpha);
    AirfoilData(:,4,iR)=Re(iR)*ones(length(Alpha),1);

    waitbar(iR/length(Re),M1);
end
close(M1)
cd(handles.dirMain)

handles.XF.AirfoilData = AirfoilData;

guidata(hObject,handles);


% --- Executes on button press in pushbutton_saveXfoil.
function pushbutton_saveXfoil_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_saveXfoil (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
temp_struct = handles.XF;
if (not(isfield(temp_struct,'AirfoilData')))
    f = warndlg('No results to save','Warning');
else
    AirfoilData = handles.XF.AirfoilData;
    default_name = ['/Table',handles.XF.Airfoil,'_N',num2str(handles.XF.Nc)];
    [file,path,indx] = uiputfile([default_name,'.mat']);
    fullFile = fullfile(path,file);
    save(fullFile,'AirfoilData')
end


function edit_Nc_Callback(hObject, eventdata, handles)
% hObject    handle to edit_Nc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_Nc as text
%        str2double(get(hObject,'String')) returns contents of edit_Nc as a double
handles.XF.Nc = str2double(get(hObject,'String'));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit_Nc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_Nc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_delta_alpha_Callback(hObject, eventdata, handles)
% hObject    handle to edit_delta_alpha (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_delta_alpha as text
%        str2double(get(hObject,'String')) returns contents of edit_delta_alpha as a double
handles.XF.dalpha = str2double(get(hObject,'String'));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit_delta_alpha_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_delta_alpha (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_max_alpha_Callback(hObject, eventdata, handles)
% hObject    handle to edit_max_alpha (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_max_alpha as text
%        str2double(get(hObject,'String')) returns contents of edit_max_alpha as a double
handles.XF.alpha_max = str2double(get(hObject,'String'));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit_max_alpha_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_max_alpha (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_min_alpha_Callback(hObject, eventdata, handles)
% hObject    handle to edit_min_alpha (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_min_alpha as text
%        str2double(get(hObject,'String')) returns contents of edit_min_alpha as a double
handles.XF.alpha_max = str2double(get(hObject,'String'));
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit_min_alpha_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_min_alpha (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_min_Re_Callback(hObject, eventdata, handles)
% hObject    handle to edit_min_Re (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_min_Re as text
%        str2double(get(hObject,'String')) returns contents of edit_min_Re as a double
handles.XF.Re_min = str2double(get(hObject,'String'));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit_min_Re_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_min_Re (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_max_Re_Callback(hObject, eventdata, handles)
% hObject    handle to edit_max_Re (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_max_Re as text
%        str2double(get(hObject,'String')) returns contents of edit_max_Re as a double
handles.XF.Re_max = str2double(get(hObject,'String'));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit_max_Re_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_max_Re (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_delta_Re_Callback(hObject, eventdata, handles)
% hObject    handle to edit_delta_Re (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_delta_Re as text
%        str2double(get(hObject,'String')) returns contents of edit_delta_Re as a double
handles.XF.dRe = str2double(get(hObject,'String'));
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit_delta_Re_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_delta_Re (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function uipanel_xfoil_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uipanel_xfoil (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


%% ========================================================================

function edit_3dR_Callback(hObject, eventdata, handles)
% hObject    handle to edit_3dR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_3dR as text
%        str2double(get(hObject,'String')) returns contents of edit_3dR as a double
handles.rotor.hub = str2double(get(hObject,'String'));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit_3dR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_3dR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_3dhole_Callback(hObject, eventdata, handles)
% hObject    handle to edit_3dhole (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_3dhole as text
%        str2double(get(hObject,'String')) returns contents of edit_3dhole as a double
handles.rotor.hole = str2double(get(hObject,'String'));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit_3dhole_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_3dhole (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_3dLoadBlade.
function pushbutton_3dLoadBlade_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_3dLoadBlade (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[Fieldfilename,Fieldpathname]=uigetfile('*.mat','Load a file :');
fullFileName = fullfile(Fieldpathname, Fieldfilename);
temp = load(fullFileName);

handles.rotor = temp.rotor;
handles.rotor.input_angle = temp.rotor.alpha;

set(handles.edit_3dNb, 'String', handles.rotor.Nb);
set(handles.edit_3dR, 'String', handles.rotor.hub);
set(handles.edit_3dhole, 'String', handles.rotor.hole);  


guidata(hObject,handles);


function edit_3dNb_Callback(hObject, eventdata, handles)
% hObject    handle to edit_3dNb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_3dNb as text
%        str2double(get(hObject,'String')) returns contents of edit_3dNb as a double
handles.rotor.Nb = str2double(get(hObject,'String'));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit_3dNb_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_3dNb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_hubFit.
function popupmenu_hubFit_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_hubFit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_hubFit contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_hubFit
handles.rotor.hubFit = get(hObject,'Value')-1;
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_hubFit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_hubFit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_hubHeight_Callback(hObject, eventdata, handles)
% hObject    handle to edit_hubHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_hubHeight as text
%        str2double(get(hObject,'String')) returns contents of edit_hubHeight as a double
handles.rotor.hubHeight = str2double(get(hObject,'String'));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit_hubHeight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_hubHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_gen3d.
function pushbutton_gen3d_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_gen3d (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Hub fitting

if handles.rotor.hubFit == 0
    r_fix = 0:10:handles.rotor.r(1)-10;
    r_total = [r_fix,handles.rotor.r];
    
    c_fix = ones(1,length(r_fix))*handles.rotor.c(1);
    c_total = [c_fix,handles.rotor.c];
    
    d_theta = (handles.rotor.theta(1)-handles.rotor.theta(2))/(handles.rotor.r(1)-handles.rotor.r(2));
    theta_fix = handles.rotor.theta(1)+d_theta*(r_fix-handles.rotor.r(1));
    theta_total = [theta_fix,handles.rotor.theta];
    
    rp = (0:0.01:1)*handles.rotor.R;
    
else
    x1 = -handles.rotor.hub/sqrt(2); x2 = 0.25*handles.rotor.R;
    y1 = handles.rotor.hub/sqrt(2); y2 = handles.rotor.c(1)/2; yp1 = 1; yp2 = 0;
    y = [y1;y2;yp1;yp2];
    
    A = [x1^3 x1^2 x1 1;
        x2^3 x2^2 x2 1;
        3*x1^2 2*x1 1 0;
        3*x2^2 2*x2 1 0];
    sol = A\y;
    
    r_fix = x1:handles.rotor.R/100:x2;
    c_fix = 2*(sol(1)*r_fix.^3+sol(2)*r_fix.^2+sol(3)*r_fix+sol(4));
    
    c_total = [c_fix,handles.rotor.c];
    r_total = [r_fix,handles.rotor.r];
    
    d_theta = (handles.rotor.theta(1)-handles.rotor.theta(2))/(handles.rotor.r(1)-handles.rotor.r(2));
    theta_fix = handles.rotor.theta(1)+d_theta*(r_fix-handles.rotor.r(1));
    theta_total = [theta_fix,handles.rotor.theta];
    c_total = [c_fix,handles.rotor.c];
    
    c_fix = c_fix(find(r_fix<=handles.rotor.hub));
    r_fix = r_fix(1:length(c_fix));
    theta_fix = theta_fix(1:length(c_fix));
    
    c_fix = (c_fix./(1+(cosd(theta_fix)-1).*((r_fix(end)-r_fix)./(r_fix(end)-r_fix(1))).^2));
    c_total(1:length(c_fix)) = c_fix;
    
    rp = (-handles.rotor.hub/handles.rotor.R/sqrt(2):0.01:1)*handles.rotor.R;    
end

cp = interp1(r_total,c_total,rp,'pchip');
thetap = interp1(r_total,theta_total,rp,'pchip');



%% Airfoil shape
airfoil = handles.f3D.Airfoil

airfoilz = airfoilShape(airfoil);


m=length(rp);
n=length(airfoilz(:,1));

handles.f3D.m = m;
handles.f3D.n = n;

x=zeros(n, m, handles.rotor.Nb);
y=zeros(n, m, handles.rotor.Nb);
z=zeros(n, m, handles.rotor.Nb);

cp = repmat(cp,n,1);

for i=1:m
    rotmat = [cosd(thetap(i)) -sind(thetap(i)); sind(thetap(i)) cosd(thetap(i))];
    rotfoil = rotmat*airfoilz';
    y(:,i,1) = cp(:,i)'.* rotfoil(1,:);
    z(:,i,1) = cp(:,i)'.* rotfoil(2,:);
end

for i=1:n; x(i,:,1)=rp';end

bladeRot = linspace(0,360,handles.rotor.Nb+1);
bladeRot(end) = [];

for i=2:handles.rotor.Nb
    temp_x = x(:,:,1);  temp_y = y(:,:,1);
    XY = [temp_x(:) temp_y(:)];
    rotmat = [cosd(bladeRot(i)) -sind(bladeRot(i)); sind(bladeRot(i)) cosd(bladeRot(i))];
    rotXY = XY*rotmat;
    
    x(:,:,i) = reshape(rotXY(:,1), size(x(:,1,1),1), []);
    y(:,:,i) = reshape(rotXY(:,2), size(x(:,1,1),1), []);
    z(:,:,i) = z(:,:,1);
end

%% ENDCAP
if strcmp(handles.f3D.Airfoil(1:6),'NACA00')
    re = airfoilz(1:round(length(airfoilz)/2),1);
    yt = airfoilz(1:round(length(airfoilz)/2),2);
    [ze,xe,ye]=cylinder(flip(yt),60);
     
    xe=xe*handles.rotor.c(end)+rp(m);
    ye=(ye-0.5)*handles.rotor.c(end);
    ze=ze*handles.rotor.c(end);
else
    nrep = 20;
    ye = repmat(airfoilz(:,1),[1,nrep])*handles.rotor.c(end); 
    factor = linspace(0,1,nrep);
    for i=1:nrep
        ze(:,i) = airfoilz(:,2)*factor(i);
    end
    ze = ze*handles.rotor.c(end);
    xe = ze*0+handles.rotor.R;    
end
[ii,jj] = size(xe);
for i=1:ii
    for j=1:jj
        yen(i,j)=cosd(thetap(m))*ye(i,j)-sind(thetap(m))*ze(i,j);
        zen(i,j)=sind(thetap(m))*ye(i,j)+cosd(thetap(m))*ze(i,j);
    end
end
for i=2:handles.rotor.Nb
    xe(end, end, i) = 0;
    yen(end, end, i) = 0;
    zen(end, end, i) = 0;
    temp_x = xe(:,:,1);  temp_y = yen(:,:,1);
    XY = [temp_x(:) temp_y(:)];
    rotmat = [cosd(bladeRot(i)) -sind(bladeRot(i)); sind(bladeRot(i)) cosd(bladeRot(i))];
    rotXY = XY*rotmat;
    
    xe(:,:,i) = reshape(rotXY(:,1), size(xe(:,1,1),1), []);
    yen(:,:,i) = reshape(rotXY(:,2), size(xe(:,1,1),1), []);
    zen(:,:,i) = zen(:,:,1);
end



% shaft/hub
nc_hub = handles.rotor.hub-handles.rotor.hole;
hub_circles = linspace(handles.rotor.hole,handles.rotor.hub,nc_hub);
[xc_temp,yc_temp,zc_temp]=cylinder(handles.rotor.hub,60);
xc_total = zeros([size(xc_temp),nc_hub]);
yc_total = xc_total; zc_total = xc_total;
for i=1:nc_hub
    [xc_temp,yc_temp,zc_temp]=cylinder(hub_circles(i),60);
    xc_total(:,:,i) = xc_temp;
    yc_total(:,:,i) = yc_temp;
    zc_total(:,:,i) = zc_temp;
end
xc = xc_total(:,:,end);
yc = yc_total(:,:,end);
zc = zc_total(:,:,end);

xch = xc_total(:,:,1);
ych = yc_total(:,:,1);
zch = zc_total(:,:,1);

% hub    
handles.f3D.xc_total = xc_total;
handles.f3D.yc_total = yc_total;
handles.f3D.zc_total = zc_total;

handles.f3D.xc = xc;
handles.f3D.yc = yc;
handles.f3D.zc = zc;
% hub hole   
handles.f3D.xch = xch;
handles.f3D.ych = ych;
handles.f3D.zch = zch;

% Blade
handles.f3D.x = x;
handles.f3D.y = y;
handles.f3D.z = z;
 
% Tip
handles.f3D.xe = xe;
handles.f3D.yen = yen;
handles.f3D.zen = zen;

% leading edge
handles.f3D.xl = rp;
handles.f3D.yl = -cp(1,:)./2.*cos(-thetap./180*pi);
handles.f3D.zl = cp(1,:)./2.*sin(-thetap./180*pi);

% trailing edge
handles.f3D.xt = rp;
handles.f3D.yt = 1*cp(1,:)./2.*cos(-thetap/180*pi);
handles.f3D.zt = -1*cp(1,:)./2.*sin(-thetap/180*pi);


% Generate 3D triangulated mesh
nZ  = 40;

xc_t = zeros(nZ, length(xc_total(1,:,1)),length(xc_total(1,1,:)) );
yc_t = xc_t; zc_t = xc_t;
xc_i = xc_t; yc_i = xc_t; zc_i = xc_t;

z_min = -handles.rotor.hubHeight/2;
z_max =  handles.rotor.hubHeight/2;
z_cut = linspace(z_min,z_max,nZ);

for i=1:nZ
    xc_t(i,:,:) = xc_total(1,:,:);
    yc_t(i,:,:) = yc_total(1,:,:);
    zc_t(i,:,:) = z_cut(i);

end

x_r = reshape(x,1,[]);
y_r = reshape(y,1,[]);
z_r = reshape(z,1,[]);
pos = find(x_r.^2+y_r.^2<handles.rotor.hole^2);
x_r(pos) = [];
y_r(pos) = [];
z_r(pos) = [];

xe_r = reshape(xe,1,[]);
yen_r = reshape(yen,1,[]);
zen_r = reshape(zen,1,[]);

xc_r = reshape(xc_t,1,[]);
yc_r = reshape(yc_t,1,[]);
zc_r = reshape(zc_t,1,[]);

xc_r_i = reshape(xc_i,1,[]);
yc_r_i = reshape(yc_i,1,[]);
zc_r_i = reshape(zc_i,1,[]);

x_r = [xc_r, x_r, xe_r];
y_r = [yc_r, y_r, yen_r];
z_r = [zc_r, z_r, zen_r];
  
handles.f3D.triMesh = alphaShape(x_r',y_r',z_r',2,'HoleThreshold',1);
figure_surfaces(hObject, eventdata, handles, 1)

guidata(hObject,handles);



% --- Executes on button press in pushbutton_saveSTL.
function pushbutton_saveSTL_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_saveSTL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (not(isfield(handles.f3D,'triMesh')))
    f = warndlg('No mesh to save','Warning');
else
    [file,path,indx] = uiputfile('PrintableRotor.stl');
    fullFile = fullfile(path,file);
    
    [T, P] = boundaryFacets(handles.f3D.triMesh)
    stlwrite(triangulation(T,P),fullFile);
end



% --- Executes during object creation, after setting all properties.
function uipanel_3D_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uipanel_3D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on selection change in popupmenu_3Dplot.
function popupmenu_3Dplot_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_3Dplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_3Dplot contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_3Dplot
plotSelection = get(hObject,'Value');

temp_struct = handles;
if (not(isfield(handles,'results')))
    f = warndlg('Generate a rotor first','Warning');
else
    if(plotSelection==1)
        axes(handles.axes1)
        figure_surfaces(hObject, eventdata, handles, 1)
        
    elseif(plotSelection==2)
        figure_surfaces(hObject, eventdata, handles, 0)

    elseif(plotSelection==3)
        axes(handles.axes1)
        plot(handles.f3D.triMesh)
        xlabel('x (mm)'); ylabel('y (mm)'); zlabel('z (mm)');
        rotate3d on
    end
end

% --- Executes during object creation, after setting all properties.
function popupmenu_3Dplot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_3Dplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function figure_surfaces(hObject, eventdata, handles, oneBlade)

if oneBlade
    nb=1;
else
    nb = handles.rotor.Nb;
end

%Figure
axes(handles.axes1)    

% hub    
surf(handles.f3D.xc, handles.f3D.yc, (handles.f3D.zc-0.5)*handles.rotor.hubHeight, ...
    'LineStyle','None','FaceAlpha',0.7);
hold on
fill3(handles.f3D.xc(1,:),handles.f3D.yc(1,:),(handles.f3D.zc(1,:)-0.5)*...
    handles.rotor.hubHeight,[0.5 0.5 0.5],'LineStyle','None','FaceAlpha',0.7)
fill3(handles.f3D.xc(2,:), handles.f3D.yc(2,:),(handles.f3D.zc(2,:)-0.5)*...
    handles.rotor.hubHeight, [0.5 0.5 0.5],'LineStyle','None','FaceAlpha',0.7)

% hole
surf(handles.f3D.xch, handles.f3D.ych,(handles.f3D.zc-0.5)*handles.rotor.hubHeight, ...
    'LineStyle','None','FaceAlpha',1);
fill3(handles.f3D.xch(1,:),handles.f3D.ych(1,:),(handles.f3D.zch(1,:)-0.5)*handles.rotor.hubHeight, ...
    [0.5 0.5 0.5],'LineStyle','None','FaceAlpha',1)
fill3(handles.f3D.xch(2,:), handles.f3D.ych(2,:),( handles.f3D.zch(2,:)-0.5)*handles.rotor.hubHeight, ...
    [0.5 0.5 0.5],'LineStyle','None','FaceAlpha',1)

% blade
for i=1:nb
    surf(handles.f3D.x(:,:,i)',handles.f3D.y(:,:,i)',...
        handles.f3D.z(:,:,i)',.1*ones(size(handles.f3D.x(:,:,i)')), ...
        'LineStyle','None');
end
colormap([1 1 1]);

%tip
for i=1:nb
    surf(handles.f3D.xe(:,:,i), handles.f3D.yen(:,:,i),...
        handles.f3D.zen(:,:,i),'LineStyle','None');
end

if(oneBlade)
    % leading edge
    plot3(handles.f3D.xl, handles.f3D.yl, handles.f3D.zl,'b-','LineWidth',2);
    
    % trailing edge
    plot3(handles.f3D.xt, handles.f3D.yt, handles.f3D.zt,'b-','LineWidth',2);
    
    % tip section
    plot3(handles.f3D.x(:,handles.f3D.m), handles.f3D.y(:,handles.f3D.m),...
        handles.f3D.z(:,handles.f3D.m),'g-','LineWidth',2);
    
    % root section
    plot3(handles.f3D.x(:,1), handles.f3D.y(:,1), handles.f3D.z(:,1),'g-','LineWidth',2);
end

% display options
axis image;
xlabel('x (mm)'); ylabel('y (mm)'); zlabel('z (mm)');
% axis off;
campos([240,-750,0]);
camtarget([24,0,0]);
camproj('orthographic');
camlight; lighting phong;
% material shiny
view([40,20]);
rotate3d on
hold off


function [airfoilz] = airfoilShape(airfoil)
%%
re1=1:-0.002:0; re2=0.002:0.002:1;

if ischar(airfoil)==1
    if(strcmp(airfoil(1:4),'NACA') && strlength(airfoil)==8)
        if strcmp(airfoil(5:6),'00')
            t = str2num(airfoil(7:end))/100;
            yt1= t/0.2*(0.2969.*sqrt(re1)-0.126.*re1-0.3516.*re1.^2+0.2843.*re1.^3-0.1015.*re1.^4);
            yt2=-t/0.2*(0.2969.*sqrt(re2)-0.126.*re2-0.3516.*re2.^2+0.2843.*re2.^3-0.1015.*re2.^4);

        else
            f = warndlg('Not suported airfoil, please load a geometry','Warning');
        end
    else
        f = warndlg('Not suported airfoil, please load a geometry','Warning');
    end
else
    half = length(airfoil(:,1))/2;
    yt1 = interp1(airfoil(1:half,1),airfoil(1:half,2),re1);
    yt2 = interp1(airfoil(half:end,1),airfoil(half:end,2),re2);
    yt2(end) = yt1(1);
end
re=[re1, re2]; yt=[yt1, yt2];
airfoil=[re' yt'];

airfoilz(:,1)=airfoil(:,1)-0.5;
airfoilz(:,2)=airfoil(:,2);

%Blade
airfoilz(:,1)=airfoil(:,1)-0.5;
airfoilz(:,2)=airfoil(:,2);



function edit51_Callback(hObject, eventdata, handles)
% hObject    handle to edit_3dairfoil (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_3dairfoil as text
%        str2double(get(hObject,'String')) returns contents of edit_3dairfoil as a double


% --- Executes during object creation, after setting all properties.
function edit51_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_3dairfoil (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_loadAirfoil.
function pushbutton_loadAirfoil_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_loadAirfoil (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[Fieldfilename,Fieldpathname]=uigetfile('*.dat','Load a file :');
fullFileName = fullfile(Fieldpathname, Fieldfilename);
handles.f3D.Airfoil = load(fullFileName);
set(handles.edit_3dairfoil, 'String', Fieldfilename);
guidata(hObject,handles);


function edit_3dairfoil_Callback(hObject, eventdata, handles)
% hObject    handle to edit_3dairfoil (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_3dairfoil as text
%        str2double(get(hObject,'String')) returns contents of edit_3dairfoil as a double
airfoil = get(hObject,'String');

if(strcmp(airfoil(1:4),'NACA') && strlength(airfoil)==8)
    if strcmp(airfoil(5:6),'00')
        handles.f3D.Airfoil = airfoil;
    else
        f = warndlg('No automatic airfoil with that name please load a geometry','Warning');
    end
else
    f = warndlg('No automatic airfoil with that name please load a geometry','Warning');   
end

guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit_3dairfoil_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_3dairfoil (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_loadairfoilXF.
function pushbutton_loadairfoilXF_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_loadairfoilXF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[Fieldfilename,Fieldpathname]=uigetfile('*.dat','Load a file :');
fullFileName = fullfile(Fieldpathname, Fieldfilename);
handles.XF.Airfoil = load(fullFileName);
set(handles.edit_airfoil, 'String', Fieldfilename);

guidata(hObject,handles);



function edit_Mach_Callback(hObject, eventdata, handles)
% hObject    handle to edit_Mach (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_Mach as text
%        str2double(get(hObject,'String')) returns contents of edit_Mach as a double
handles.XF.Mach = str2double(get(hObject,'String'));
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit_Mach_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_Mach (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
