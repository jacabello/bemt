clear all; close all;

%% OPTIONS FOR CALCULATION
Plot_Geometry = 1;    %  Plot and save geometry if true
GermanFixed = 1;      % Equivalent geometry for german fixed wing


%% ROTOR INPUT PARAMETERS
rotor.R = 80;     % rotor radius (mm)
rotor.r = linspace(0.2*rotor.R+rotor.R/100,rotor.R-rotor.R/100,100);   %Radius distribution
rotor.c = 26*ones(1,length(rotor.r));   %chord distribution
rotor.hub = 7.5;    %hub radious (mm)
rotor.Nb = 1;  

%Number of Blades
alpha0 = 6;   %Geometric angle of atack


%% OPTIONS PARAMETERS
Options.tol = 1e-16;      %Tolerance for the loop
Options.TipC = 0;         %Tip Correction 1 true
Options.Corr_3D = 0;      %3D correction
Options.relax = 0.35;     %Relaxation factor
Options.Limit = 1000;  
Options.Mode = 'Design';   


%% FLOW PARAMETERS
flow.nu = 1;              % kinematic viscosity (mm^2/s)
flow.Vo = 0;             %ascending velocity (mm/s)
flow.Re = 8e4;             %Reynolds number


%% CALCULATED SIMULATION PARAMETERS
flow.sigma = rotor.Nb*rotor.c/pi/rotor.R;            % tip solidity
flow.omega = flow.Re*flow.nu/(rotor.c(end)*rotor.R);     %Omega to get the aproximate reynodls number
flow.Lambda_c = flow.Vo/flow.omega/rotor.R;        %free-stream velocity ratio (mm/s)
f = flow.omega/2/pi;

TSR= 2*pi*rotor.R*f/flow.Vo;             % free-stream velocity (mm/s)


%% HUB FITTING
x1 = -rotor.hub/sqrt(2); x2 = 0.25*rotor.R; y1 = rotor.hub/sqrt(2); ...
    y2 = rotor.c(1)/2; yp1 = 1; yp2 = 0;
y = [y1;y2;yp1;yp2];

A = [x1^3 x1^2 x1 1; 
    x2^3 x2^2 x2 1;
    3*x1^2 2*x1 1 0;
    3*x2^2 2*x2 1 0];
sol = A\y;

r_fix = x1:rotor.R/100:x2;
c_fix = 2*(sol(1)*r_fix.^3+sol(2)*r_fix.^2+sol(3)*r_fix+sol(4));

c_total = [c_fix,rotor.c];
r_total = [r_fix,rotor.r];


%% Load tabulated data from Xfoil
XF = load('AirfoilsData/TableNACA0012_N5.mat');
Re_idx = round(flow.Re/1e4+1);
% NACA0012(1,...) --> Re = 0, NACA0012(2,...)-->Re = 1e4 and so on
lift_alpha_x = squeeze(XF.Airfoil(Re_idx,1,:));
lift_cL_x = squeeze(XF.Airfoil(Re_idx,2,:));

if GermanFixed
    %% EQUIVALENT GERMAN FIXED WING INPUT PARAMETERS
    b = 290;       %Complete wing span
    c_f = 100;     %Chord
    U = flow.Re*flow.nu/c_f; %Incoming velocity
    AR = b/c_f*2;  %Aspect ratio
    
    [GAMMA_x, alpha_eff_x,r_x] =Prandtl_fix(b,c_f,alpha0,U,lift_alpha_x,lift_cL_x);
    alpha_eff_x(end) = 0;  alpha_eff_x(1) = 0;
    y = r_x/2/b;
    
    %% Effective wing angle of attack fit on rotor geometry
    alpha_rx = RotorFit(y,AR,alpha_eff_x,rotor.r,mean(rotor.c)); %NACA lift xfoil
    
    %% Figure for circulation and efective angle of attack distributions
    figure(1)
    subplot(1,2,1)
    plot(y*2*b/c_f,180*alpha_eff_x/pi,[-0.5,3],[alpha0 alpha0],'k--','Linewidth',2);
    xlabel('$Y/c$','interpreter','Latex','FontSize',16)
    ylabel('$\alpha(^\circ)$','Interpreter','Latex','FontSize',16)
    legend('Effective $\alpha$','Geometric $\alpha$','interpreter','latex',...
        'location','southwest')
    title('\textbf{Fixed wing}','interpreter','latex','fontsize',14)
    xlim([0 b/c_f])
    ylim([0 alpha0+2])
    
    subplot(1,2,2)
    hold off
    plot(rotor.r/rotor.c(end),180*alpha_rx/pi,[-0.5,5],[alpha0 alpha0],'k--','Linewidth',2);
    hold on
    plot((rotor.R/mean(rotor.c)-AR/2)*[1,1],[0,12],':','linewidth',2)
    xlabel('$r/c$','interpreter','Latex','FontSize',16)
    ylabel('$\alpha(^\circ)$','Interpreter','Latex','FontSize',16)
    text(2.5,4,'$\alpha = \alpha(Y)$','interpreter','Latex','fontsize',16)
    text(0.2,4,'$\alpha = \alpha(0)$','interpreter','Latex','fontsize',16)
    title('\textbf{Rotor blade}','interpreter','latex','fontsize',14)
    xlim([0 rotor.R/rotor.c(end)])
    ylim([0 alpha0+2])
    
    xlabel('$Y$','interpreter','Latex','FontSize',18)
    ylabel('$\Gamma$','Interpreter','Latex','FontSize',18)
    
    rotor.input_angle = ones(1,length(rotor.r)).*alpha_rx*pi/180;
    
else
    rotor.input_angle = ones(1,length(rotor.r))*alpha0;
end


%% BEMT calculation for NACA
Options.Lambda_i_guess = 0.1;
Options.ap_guess = 0;
[results] = BEMT(rotor, flow, XF.Airfoil, Options);
%%
figure(2)
rn = rotor.r/rotor.R;
plot(rn,results.theta,rn(results.errors),...
    (1-isnan(results.theta(results.errors))).*results.theta(results.errors),...
    's','linewidth',2)
xlabel('$r/R$','interpreter','Latex','FontSize',18)
ylabel('$\theta$','interpreter','Latex','FontSize',18)

figure(3)
plot(rn,results.gamma/100,'linewidth',2)
xlabel('$r/R$','interpreter','Latex','FontSize',18)
ylabel('$\Gamma (cm^2/s)$','interpreter','Latex','FontSize',18)


Re = rotor.c(end) * flow.omega*rotor.R*sqrt((results.Lambda_i(end-1)+...
    flow.Lambda_c)^2+1)/flow.nu;
h = 1/2*flow.omega*(flow.Lambda_c+max(results.Lambda_i(1:end-1)))/f;

disp(['Re = ' num2str(Re) ' (Actual Reynolds number on the tip)'])
disp(['h/R = ' num2str(h) ' (predicted helix pitch)'])
pause(0.)

%%
figure(5)
plot(rn,results.Lambda_i,rn,results.ap,'linewidth',2)
xlabel('r/R','interpreter','Latex','Fontsize',14)
ylabel('Indeced Velocity factors','interpreter','LaTex','Fontsize',14)
legend({'$\lambda_i$','$a_p$'},'location','best',...
    'Interpreter','Latex','Fontsize',14)