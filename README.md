# BEMT

<img src="https://gitlab.com/jacabello/bemt/-/raw/master/showAPP.PNG" width="80%"/>

GUI-based tool to easily design and 3D print rotor blades acording to Blade Element Momentum Theory (BEMT).

Many of the ideas for this implementation are taken from the book: Principles of helicopter aerodynamics by J. Gordon Leishman. This BEMT formulation allows to compute both helicopter and wind mill regimes withing the validity range of BEMT.


## Design and calculation

Design a blade to have a certain angle of attack along the span or used a defined blade geometry to calculate how it will perform under different flow situations according to BEMT.

### Design parameters

You can widley define the geometry of the rotor and use the BEMT method to obtain the twist angle that will fullfill the required angle of attack of the blade along the span for the required flow conditions. you can use constan values or load any distribution for the chord and angle of attack profiles.

### Calculation parameters

It is only needed to load a given rotor geometry (including the twist angle of the blades), so the BEMT can be used to calculate the performance under different working conditions.

### Flow parameters

This options set the working conditions to make the calculations. You can set the velocity of the flow, frequency of the rotor, etc.

Aerodynamic coefficients for 2D airfoils are also needed. Thos program use the ideal flow theory by default, but it is also possible to load different table of aerodynamic coefficent data to interpolate on them according to AoA and Reynolds number.

## XFoil

There is included the feature of generate your own aerodynamic coefficient data using the software [XFoil](https://web.mit.edu/drela/Public/web/xfoil/). The interface with matlab is the [Xfoil Interface](https://www.mathworks.com/matlabcentral/fileexchange/49706-xfoil-interface-updated) developed by Louis Edelman.

## 3D Blade

This tool allows to visualize the 3D rotor given by the calculated geometry. This menu also alow to modify the rotor changing the hub size, the screw hole or the 2D airfoil geometry. Finally once the 3D object is created, it can be saved in a .stl file to be easily printed.
