function [Gamma,alpha,r] = Prandtl_fix(R,c,alpha0,U,lift_alpha,lift_cL)

m = 100;    %Number of blade elements on the iterative scheme

dr = R/m;
r  = -R:dr:R;
ri = -R+dr/2:dr:R-dr/2;
n = length(r);

eps_change = 0.01;     % fraction of calculated change in Gamma applied for next iteration
eps_stop = 10^(-8);     % iteration stops whan max. change in Gamma < this value
max_iterations = 5000;  % max. number of iterations

Gamma = zeros(1,length(r));
remainder_int = zeros(1,n);

alpha0 = alpha0/180*pi;
alpha = alpha0*zeros(1,n);
lift_alpha = lift_alpha/180*pi;


for i=1:max_iterations
    Gamma_old = Gamma;
    dGdri = diff(Gamma)/dr;
    dGdr = interp1(ri,dGdri,r,'spline');
    
    for j=1:n
        integrand=(dGdri-dGdr(j))./(r(j)-ri);
        remainder_int(j) = sum(integrand)*dr;
    end

    Uz = -1/4/pi.*(dGdr.*(log(R+r)-log(R-r)) + remainder_int);
    alpha = alpha0+atan(Uz./U);
    alpha(1) = 0;
    alpha(end) = 0;
    
    cL = interp1(lift_alpha,lift_cL,alpha,'linear');
    
    %Total velocity
    Ut = sqrt(U.^2+Uz.^2);    
    
    %Circulation
    Gamma = 1/2*cL.*c.*Ut;
    Gamma(1) = 0;
    Gamma(n) = 0;
    
    change = Gamma - Gamma_old;
    Gamma  = Gamma_old + eps_change*change;

    max_change(i) = max(abs(change));
    if max_change(i)<eps_stop, break, end
end

end



