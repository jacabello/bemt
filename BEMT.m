% function [alpha,theta,Lambda_i,ap,gamma,phi,cL,errors,Lambda_i_m,ap_m,Ct] = BEMT(Lambda_c,input_angle,r,omega,R,...
%     c,nu,sigma,Nb,hub,Lambda_i_guess,ap_guess,Airfoil,t_c,Options)
function [results] = BEMT(rotor, flow, Airfoil, Options)
%Blade Element Momethum Theory method for rotor blades calculation.

% - Lambda_c = Non dimensional ascending velocity
% - theta = Twist angle distribution of the blade(deg)
% - theta = effective angle of attack distribution(deg)
% - r = Radial distribution of the blade (mm)
% - c = Blade Chord distribution
% - omega = Angular velocity (rad/s)
% - R = Maximun blade Radious
% - nu = Fluid kinematic viscossity (mm²/s)
% - sigma = solidity at the tip 
% - Nb = Number of blades
% - Arfoil = Lookup table for aerodynamic profile Lift and Drag
% - TipC = Tip correction options
% - Corr_3D = 3D correction option

input_angle = rotor.input_angle;
r = rotor.r;
R = rotor.R;
c = rotor.c;
Nb = rotor.Nb;
hub = rotor.hub;

Lambda_c = flow.Lambda_c;
omega = flow.omega;
sigma = flow.sigma;
nu = flow.nu;

t_c = 0.15
% Lambda_i_guess = Options.Lambda_i_guess;
% ap_guess = Options.ap_guess;

WaitBar = waitbar(0,'Calculation in process');

%Initialize data
cont_err = 0;
errors = 1;

alpha = zeros(1,length(input_angle));
theta = alpha;
phi = alpha;
gamma = alpha;

Lambda_i = [zeros(size(r)),Options.Lambda_i_guess];
ap = [zeros(size(r)), Options.ap_guess];

%Backward loop
for n=length(r):-1:1
    Lambda_i(n) = Lambda_i(n+1);
    ap(n) = ap(n+1);
    err = 1; err1 = 1;
    cont = 0;
    while (abs(err)>Options.tol | abs(err1)>Options.tol) & cont < Options.Limit
        
        cont = cont+1;
        phi(n) = atan(R*(Lambda_i(n)+Lambda_c)/(r(n)*(1+ap(n))));
        
        if strcmp(Options.Mode,'Calculation')
            %Input =theta;
            theta(n) = input_angle(n);
            alpha(n) = theta(n)-phi(n)*180/pi;
        elseif strcmp(Options.Mode,'Design')
            %Input=alpha
            alpha(n) = input_angle(n);
            theta(n) = phi(n)*180/pi+alpha(n);
        else
            errordlg(['"',Options.Mode,'"',' is not a recognise option. Available modes are: "Calculation" and "Design"'])
            return;
        end
        
        if Options.TipC   %Tip correction factor [Glauert 1935]
            f = Nb/2* (R-r(n))/(r(n)*sin(abs(phi(n))));
            F = (2/pi)*acos(exp(-abs(f)));
            if Options.TipC == 2  %Tip and hub correction [Emmanuel Branlard Book]
                F_hub = 2/pi*acos(exp(-abs(1/2*(r(n)-hub)/(hub*sin(phi(n))))));
                F = F*F_hub;
            elseif Options.TipC == 3  %Tip correction factor [Betz 1919 & Prandtl 1921]
                f = -Nb/2*(1-r(n)/R)*sqrt(1+((1+ap(end))/(Lambda_c+Lambda_i(end)))^2);
                F = 2/pi * acos(exp(f));
            end
        else  %Without tip correction factor
            F = 1;
        end

        %Lift and Drag Data
        Re = c(n)*sqrt((omega*R*(Lambda_i(n)+Lambda_c))^2+(omega*r(n)*(1+ap(n)))^2)/nu;
       
        [D,L] = DragLift(alpha(n),Re,Airfoil);
        cL(n) = L; cD(n) = D;
        
        %3D Correction
        if Options.Corr_3D == 1
           cL(n) = Correction3D(L,alpha(n),r(n),c(n),Re,Airfoil,t_c); 
        end
 
        %Force coefficients
        Cz(n) = cL(n)*cos(phi(n))-cD(n)*sin(phi(n));
        Cx(n) = cL(n)*sin(phi(n))+cD(n)*cos(phi(n));
        
        %store previus value
        Lambda_i_prev = Lambda_i(n);
        ap_prev = ap(n);
        
        %Sign change if wind turbine condtions are calculated
        Sign = sign(Lambda_c+Lambda_i(n));
        
        %Solve equation of Axial induced velocity
%         if Options.WakeRot  
            %Wake rotation [Burton et al.(2002) & Sorensen Book]
%             Lambda_i(n) = Sys_Wake(Lambda_c,sigma(n),r(n)/R,Cz(n),Cx(n),phi(n),F,Sign);
%         else   
            %Usual algorithm from Leishman and sorensen (no wake rotation)
            Lambda_i(n) = EcSys(Lambda_c,sigma(n),r(n)/R,Cz(n),Cx(n),phi(n),F,Sign);
%         end

        %Implement relaxation and ap
        [Lambda_i(n),ap(n)] =  Relaxation(Lambda_i(n),Lambda_c,Lambda_i_prev,r(n)/R,sigma(n),Cx(n),phi(n),Options.relax,F,Sign);
        
        %Error
        err = Lambda_i(n)-Lambda_i_prev;
        err1 = ap(n)-ap_prev;
       
        %Mean Induction along the anulus
        [Lambda_i_m(n),ap_m(n)] = MeanInd(Lambda_c,Lambda_i(n),ap(n),F,Sign);

        %Circulation
        Veff(n) = sqrt(omega^2*R^2*(Lambda_i(n)+Lambda_c).^2+(omega*r(n)+omega*r(n)*ap(n)).^2);
        gamma(n) = 0.5*cL(n)*Veff(n)*c(n);  
    end
    
    if cont >= Options.Limit | isnan(Lambda_i(n))
        cont_err = cont_err+1;
        errors(cont_err) = n;
        if n == length(n) | isnan(Lambda_i(n)) | isnan(ap(n));
            Lambda_i(n) = Lambda_i(n+1);
            ap(n) = ap(n+1);
        end
    end
    waitbar((length(r)-n)/length(r),WaitBar);
    
end
Lambda_i = Lambda_i(1:end-1);
ap = ap(1:end-1);

Ct = RotorForces(r/R,sigma,Lambda_i,Lambda_c,ap,Cx,Cz);

delete(WaitBar);

%Save variables in a class
results.alpha = alpha;
results.theta = theta;
results.Lambda_i = Lambda_i;
results.ap = ap;
results.gamma = gamma;
results.phi = phi;
results.cL = cL;
results.errors = errors;
results.Lambda_i_m = Lambda_i_m;
results.ap_m = ap_m;
results.Ct = Ct;
end

%% Drag and Lift Interpolation
function [cD,cL] = DragLift(alpha,Re,Airfoil)
if size(size(Airfoil),2) == 3
    %Interpolate data to the design Re along the tip and AoA
    cL = interp2(squeeze(Airfoil(:,1,:)),squeeze(Airfoil(:,4,:)),squeeze(Airfoil(:,2,:)),alpha,Re,'spline');
    cD = interp2(squeeze(Airfoil(:,1,:)),squeeze(Airfoil(:,4,:)),squeeze(Airfoil(:,3,:)),alpha,Re,'spline');
else
    %Constant Reynolds number
    cL = interp1(squeeze(Airfoil(:,1)),squeeze(Airfoil(:,2)),alpha,'spline');
    cD = interp1(squeeze(Airfoil(:,1)),squeeze(Airfoil(:,3)),alpha,'spline');
end
end


%% 3D  Correction [Snel et al]
function cL = Correction3D(cL_2D,alpha,r,c,Re,Airfoil,t_c) %[Snel et al, 1993]
    d= 3.1; e=2;
%     if size(size(Airfoil),2) == 3  
%         cLa = interp2(squeeze(Airfoil(:,1,:)),squeeze(Airfoil(:,4,:)),...
%             squeeze(Airfoil(:,2,:)),[alpha-0.5,alpha+0.5],Re,'linear'); 
%     else
%         cLa = interp1(squeeze(Airfoil(:,1)),squeeze(Airfoil(:,2)),...
%             [0,1],'spline');
%     end
%     cL_alpha = (cLa(2)-cLa(1))/1/pi*180;
%     cL_inv = cL_alpha*(sind(alpha));

    cL_inv = 2*pi*(1+0.77*t_c)*alpha*pi/180;  %[Abbot and Von Doenhoff, 1959]
    cL = cL_2D+tanh(d*(c/r)^e)*(cL_inv-cL_2D);  %[Snel et al, 1993]
end

%% Induced axial velocity
function [Lambda_i] = EcSys(Lambda_c,sigma,r,Cz,Cx,phi,F,Sign)
Lambda_i = (r+sigma*Lambda_c*Cx/(8*F*r*sin(phi)^2))/...
    (Sign*F*8*r/sigma/Cz*sin(phi)*cos(phi)-sigma*Cx/(8*F*r*sin(phi)^2));
end

%% Wake rotation solver
function [Lambda_i] = Sys_Wake(Lambda_c,sigma,r,Cz,Cx,phi,F,Sign)
k1 = F*8*sin(phi)*cos(phi)/sigma/Cz;
k2 = Cx*sigma/(8*r^2*sin(phi)^2);

Lambda_i = (1+Lambda_c*k2-Sign*Lambda_c*r^2*k1*k2^2)/(Sign*k1+Sign*r^2*k1*k2^2-k2);
end

%% Relaxation and induced swirl
function  [Lambda_i,ap] = Relaxation(Lambda_i,Lambda_c,Lambda_i_prev,r,sigma,Cx,phi,f,F,Sign)
Lambda_i = Lambda_i*f+(1-f)*Lambda_i_prev;
ap = Cx*sigma*(Lambda_i+Lambda_c)/(8*F*r^2*sin(phi)^2);
end

%% Mean induced velocity
function [Lambda_i_m,ap_m] = MeanInd(Lambda_c,Lambda_i,ap,F,Sign)
Lambda_i_m = (-Lambda_c+Sign*sqrt(Lambda_c^2+4*Lambda_i*F*(Lambda_c+Lambda_i)))/2;
ap_m = ap*F*(Lambda_c+Lambda_i)/(Lambda_c+Lambda_i_m);
end


%% Thrusr
function [dCt] = RotorForces(r,sigma,Lambda_i,Lambda_c,ap,Cx,Cz)
    Cz(isnan(Cz)) = 0; Cz(find(Cz<0)) = 0;
    Lambda_i(isnan(Lambda_i)) = 0;
    ap(isnan(ap)) = 0;
    dCt = sigma/2.*((Lambda_i+Lambda_c).^2+(r+r.*ap).^2).*Cz;
end