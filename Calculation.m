clear all; close all;

%% Blade Geometry
% Blade = load('BladeGeometry/data_AR3.mat');
Blade = load('BladeGeometry/Blade_calculation');
rotor = Blade.rotor;

%% OPTION PARAMETERS

Options.tol = 1e-16;      %Tolerance for the loop
Options.TipC = 0;         %Tip Correction 1:Glauert, 2:Hub, 3:Betz&Prandtl
Options.Corr_3D = 0;      %3D correction
Options.WakeRot = 0;      %Wake rotation model
Options.relax = 0.3;     %Relaxation factor
Options.Limit = 1000;
Options.Mode = 'Calculation';   


%% FLOW PARAMETERS
flow.nu = 1;              % kinematic viscosity (mm^2/s)
flow.Vo = 0;             %ascending velocity (mm/s)
flow.Re = 8e4;             %Reynolds number

%% CALCULATED SIMULATION PARAMETERS
flow.sigma = rotor.Nb*rotor.c/pi/rotor.R;            % tip solidity
flow.omega = flow.Re*flow.nu/(rotor.c(end)*rotor.R);     %Omega to get the aproximate reynodls number
flow.Lambda_c = flow.Vo/flow.omega/rotor.R;        %free-stream velocity ratio (mm/s)
f = flow.omega/2/pi;
TSR= 2*pi*rotor.R*f/flow.Vo;             % free-stream velocity (mm/s)

%% Tabulated data from Xfoil
XF = load('AirfoilsData/TableWindTunel');
XF.t_c = 0.12;   %Thickness ratio

%% BEMT Calculator
Options.Lambda_i_guess = 0.1;
Options.ap_guess = 0;
[results] = BEMT(rotor, flow, XF.Airfoil, Options);

rn = rotor.r/rotor.R;
%%
hold off
figure(1)
plot(rn,results.alpha,rotor.r(results.errors)/rotor.R,...
    (1-isnan(results.alpha(results.errors))).*results.alpha(results.errors),...
    's','linewidth',2)
xlabel('r/R')
ylabel('\alpha')
ylim([0,12])
legend({'$BEMT$','Not converged'},'interpreter','latex','fontsize',13,...
    'location','best')

%%
hold off
figure(2)
plot(rn,results.gamma/100,'linewidth',2)
xlabel('r/R')
ylabel('\Gamma(cm^2/s)')

%%
figure(3)
subplot(1,2,1)
plot(rn,results.Lambda_i,rn,results.ap,'linewidth',2)
xlabel('r/R')
ylabel('Blade Induced Velocitys')
legend('\lambda_i','ap')
xlim([0,1])
ylim([0 0.25])

subplot(1,2,2)
plot(rn,results.Lambda_i_m,rn,results.ap_m,'linewidth',2)
xlabel('r/R')
ylabel('Anulus Mean Induced Velocitys')
legend('\lambda_i_m','ap_m')
xlim([0,1])
ylim([0 0.25])

%%
figure(4)
plot(rn,results.cL,'linewidth',2)
xlabel('$r/R$','interpreter','latex','fontsize',16)
ylabel('$C_L$','interpreter','latex','fontsize',16)
ylim([0,1])

%%
figure(5)
plot(rn,results.Ct,'linewidth',2)
xlim([0,1])
xlabel('$r/R$','interpreter','latex','fontsize',16)
ylabel('$dCt/dr$','interpreter','latex','fontsize',16)


%Flow rate
dr = rotor.r(2)-rotor.r(1);
Qr = dr/10*trapz(2*pi*rotor.r./10.*results.Lambda_i.*flow.omega*rotor.R/10); %Flow rate
Ar = pi*rotor.R^2;
As = 45*38;  %Water tunnel Area
disp(['Flow-Rate = ' num2str(Qr) ' (cm^3/s)'])
disp(['Qr/A = ' num2str(Qr/As) ' (cm/s)'])
disp(['Vr = ' num2str(Qr/(pi*(rotor.R/10)^2)) ' (cm/s)'])