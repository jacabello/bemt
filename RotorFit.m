function alpha_r = RotorFit(y,AR,alpha_eff,r,c)
% Y = non dimensional span of fixed wing
% AR = aspect ratio of fixed wing (2xSAR)
% c = chord of rotor

r_fix = y*AR*c;
i = find(r<max(r_fix));

alpha_tip = interp1(r_fix,alpha_eff,r(i));


%Calculate angle distribution for rotor
alpha_r = zeros(1,length(r));
alpha_r(end-length(alpha_tip)+1:end) = alpha_tip;
alpha_r(1:end-length(alpha_tip)) = alpha_tip(1);